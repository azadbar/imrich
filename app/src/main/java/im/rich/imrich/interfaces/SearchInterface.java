package im.rich.imrich.interfaces;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import im.rich.imrich.retrofit.resObject.SearchResponse;

/**
 * Created by m.azadbar on 2/28/2018.
 */

public interface SearchInterface {


    void successSearch(Context currentContext, ArrayList<String> list);

    void success(List<String> list);
}
