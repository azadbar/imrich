package im.rich.imrich.presenter;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import im.rich.imrich.activities.SearchActivity;
import im.rich.imrich.interfaces.SearchInterface;
import im.rich.imrich.model.PojoModel;
import im.rich.imrich.retrofit.resObject.SearchResponse;

/**
 * Created by m.azadbar on 2/28/2018.
 */

public class SearchPresenter {

    private SearchActivity view;
    private SearchInterface richInterface;

    public SearchPresenter(SearchActivity rView, SearchInterface richInterface) {
        this.view = rView;
        this.richInterface = richInterface;
    }


    public void callWebAPI() {
        new PojoModel(this).callSearchApi();
    }



    public void setList(Context currentContext, ArrayList<String> list) {
        richInterface.successSearch(currentContext, list);
    }

    public void successSearch(List<String> list) {
        richInterface.success(list);
    }
}
