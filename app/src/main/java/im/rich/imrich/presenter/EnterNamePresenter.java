package im.rich.imrich.presenter;

import im.rich.imrich.fragments.EnterNameFragment;
import im.rich.imrich.interfaces.EnterNameInterface;
import im.rich.imrich.model.PojoModel;
import im.rich.imrich.retrofit.resObject.SearchResponse;
import im.rich.imrich.retrofit.resObject.StartPageResponse;

/**
 * Created by mohammad on 2/20/2018 AD.
 */

public class EnterNamePresenter {

    private EnterNameInterface enterNameInterface;
    private EnterNameFragment view;

    public EnterNamePresenter( EnterNameFragment view, EnterNameInterface enterNameInterface) {
        this.enterNameInterface = enterNameInterface;
        this.view = view;
    }

    public void callWepApi(String mac, String name) {
        new PojoModel(this).callWebApi(mac,name);
    }

    public void successLogin(StartPageResponse obj) {
        enterNameInterface.StartPageSuccess(obj);
    }

    public void callSearchApi() {
        new PojoModel(this).callEnterName();
    }

//    public void successSearch(SearchResponse obj) {
//        this.enterNameInterface.successResponse(obj);
//    }
}
