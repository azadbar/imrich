package im.rich.imrich.controller;

import android.app.Activity;
import android.app.Application;
import android.content.Context;



public class AppController extends Application {

    private static Activity CurrentActivity = null;
    private static Context CurrentContext;

    public void onCreate() {
        super.onCreate();



    }

    protected void attachBaseContext(Context base) {

        super.attachBaseContext(base);
    }

    public static void setCurrentContext(Context mCurrentContext) {
        CurrentContext = mCurrentContext;
    }

    public static void setActivityContext(Activity activity, Context context) {
        CurrentActivity = activity;
        CurrentContext = context;
    }

    public static Activity getCurrentActivity() {
        return CurrentActivity;
    }

    public static Context getCurrentContext() {
        return CurrentContext;
    }

}
