package im.rich.imrich.helper;

import android.content.Context;
import android.preference.PreferenceManager;

import im.rich.imrich.controller.AppController;


public class PreferencesData {

    public static Context context = null;

    public static Context getContext() {
        if (context != null) {
            return context;
        } else {
            return AppController.getCurrentContext();
        }
    }

    public static void saveString(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString(key, value).commit();
    }
    public static void saveBitmap(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString(key , value).commit();
    }
    public static String getBitmap(String key) {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getString(key, "");
    }



    public static void saveFloat(String key, Float value) {
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putFloat(key, value.floatValue()).commit();
    }

    public static void saveInt(String key, int value) {
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putInt(key, value).commit();
    }

    public static void saveLong(String key, long value) {
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putLong(key, value).apply();
    }

    public static void saveBoolean(String key, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putBoolean(key, value).apply();
    }

    public static int getInt(String key) {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(key, 0);
    }

    public static String getString(String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getString(key, defaultValue);
    }

    public static String getString(String key) {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getString(key, "");
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(key, defaultValue);
    }

    public static float getFloat(String key, float defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getFloat(key, defaultValue);
    }

    public static long getLong(String key, long defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getLong(key, defaultValue);
    }




}
