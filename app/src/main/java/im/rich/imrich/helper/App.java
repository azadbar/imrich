package im.rich.imrich.helper;

import android.app.Application;

import com.onesignal.OneSignal;

/**
 * Created by mohammad on 3/4/2018 AD.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }
}
