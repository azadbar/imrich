package im.rich.imrich.helper;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.UUID;


public class MacAddress  {
    public static String getMacAddress(Context context) {

        String macAddress = null;

            try {
                try {
                    String interfaceName = "wlan0";
                    List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                    for (NetworkInterface intf : interfaces) {
                        if (!intf.getName().equalsIgnoreCase(interfaceName)){
                            continue;
                        }

                        byte[] mac = intf.getHardwareAddress();
                        if (mac==null){
                            return "";
                        }

                        StringBuilder buf = new StringBuilder();
                        for (byte aMac : mac) {
                            buf.append(String.format("%02X:", aMac));
                        }
                        if (buf.length()>0) {
                            buf.deleteCharAt(buf.length() - 1);
                        }
                        macAddress= buf.toString();
                    }
                } catch (Exception ex) { }
//                return  "35" + //we make this look like a valid IMEI
//                        Build.BOARD.length()%10+ Build.BRAND.length()%10 +
//                        Build.CPU_ABI.length()%10 + Build.DEVICE.length()%10 +
//                        Build.DISPLAY.length()%10 + Build.HOST.length()%10 +
//                        Build.ID.length()%10 + Build.MANUFACTURER.length()%10 +
//                        Build.MODEL.length()%10 + Build.PRODUCT.length()%10 +
//                        Build.TAGS.length()%10 + Build.TYPE.length()%10 +
//                        Build.USER.length()%10 ; //13 digits



            } catch (Exception ex) {
                Log.e("TAG",ex.getMessage());
            }


        if(macAddress==null)
            macAddress=pSudo();

        return macAddress;
    }

    private static String pSudo(){
        String m_szDevIDShort = "35" +
                (Build.BOARD.length() % 10)
                + (Build.BRAND.length() % 10)
                + (Build.CPU_ABI.length() % 10)
                + (Build.DEVICE.length() % 10)
                + (Build.MANUFACTURER.length() % 10)
                + (Build.MODEL.length() % 10)
                + (Build.PRODUCT.length() % 10);

        // Thanks to @Roman SL!
        // http://stackoverflow.com/a/4789483/950427
        // Only devices with API >= 9 have android.os.Build.SERIAL
        // http://developer.android.com/reference/android/os/Build.html#SERIAL
        // If a user upgrades software or roots their phone, there will be a duplicate entry
        String serial = null;
        try
        {
            serial = Build.class.getField("SERIAL").get(null).toString();

            // Go ahead and return the serial for api => 9
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        }
        catch (Exception e)
        {
            // String needs to be initialized
            serial = "serial"; // some value
        }

        // Thanks @Joe!
        // http://stackoverflow.com/a/2853253/950427
        // Finally, combine the values we have found by using the UUID class to create a unique identifier
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }


}