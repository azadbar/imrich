package im.rich.imrich.fragments;


import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.rich.imrich.R;
import im.rich.imrich.activities.SearchActivity;
import im.rich.imrich.presenter.SearchPresenter;

public class RichFragment extends Fragment {

    @BindView(R.id.imgSearch)
    AppCompatImageView imgSearch;
    @BindView(R.id.btnRich)
    AppCompatImageView btnRich;
    @BindView(R.id.imgDiamond)
    AppCompatImageView imgDiamond;
    @BindView(R.id.imgShare)
    AppCompatImageView imgShare;
    @BindView(R.id.root)
    RelativeLayout root;

    private SearchPresenter mRichPresenter;
    public List<String> search;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rich, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @OnClick({R.id.imgSearch, R.id.btnRich, R.id.imgShare})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSearch:
//                mRichPresenter.callWebAPI();
                Intent intent = new Intent(getContext(), SearchActivity.class);
                getContext().startActivity(intent);
                break;
            case R.id.btnRich:
                getFragmentManager().beginTransaction().replace(R.id.root, new EnterNameFragment()).addToBackStack("entername").commit();
                break;
            case R.id.imgShare:
                shareApplication();
                break;
        }
    }

    public void shareApplication() {
        ApplicationInfo app = getActivity().getApplicationContext().getApplicationInfo();
        String filePath = app.sourceDir;

        Intent intent = new Intent(Intent.ACTION_SEND);

        // MIME of .apk is "application/vnd.android.package-archive".
        // but Bluetooth does not accept this. Let's use "*/*" instead.
        intent.setType("*/*");

        // Append file and send Intent
        File originalApk = new File(filePath);

        try {
            //Make new directory in new location
            File tempFile = new File(getActivity().getExternalCacheDir() + "/ExtractedApk");
            //If directory doesn't exists create new
            if (!tempFile.isDirectory())
                if (!tempFile.mkdirs())
                    return;
            //Get application's name and convert to lowercase
            tempFile = new File(tempFile.getPath() + "/" + getActivity().getString(app.labelRes).replace(" ","").toLowerCase() + ".apk");
            //If file doesn't exists create new
            if (!tempFile.exists()) {
                if (!tempFile.createNewFile()) {
                    return;
                }
            }
            //Copy file to new location
            InputStream in = new FileInputStream(originalApk);
            OutputStream out = new FileOutputStream(tempFile);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            System.out.println("File copied.");
            //Open share dialog
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(tempFile));
            getActivity().startActivity(Intent.createChooser(intent, "Share app via"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void successSearch(SearchResponse obj) {
//
//        Intent intent = new Intent(getContext(), SearchActivity.class);
//        intent.putExtra("object", obj);
//        getContext().startActivity(intent);
//    }
}
