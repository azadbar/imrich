package im.rich.imrich.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.rich.imrich.R;
import im.rich.imrich.activities.SearchActivity;

public class EnterBankAcountFragment extends Fragment {


    @BindView(R.id.imgSearch)
    AppCompatImageView imgSearch;
    @BindView(R.id.tvEnterBankInfo)
    AppCompatTextView tvEnterBankInfo;
    @BindView(R.id.tvSendMoney)
    AppCompatTextView tvSendMoney;
    @BindView(R.id.imgArrowRight)
    AppCompatImageView imgArrowRight;
    @BindView(R.id.main)
    RelativeLayout main;
    @BindView(R.id.imgShare)
    AppCompatImageView imgShare;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.rlSendMoney)
    RelativeLayout rlSendMoney;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_bank_account_page, container,false);

        ButterKnife.bind(this, view);
        return view;
    }




    @OnClick({R.id.imgSearch, R.id.tvEnterBankInfo, R.id.main, R.id.imgShare, R.id.rlSendMoney})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSearch:
                Intent intent = new Intent(getContext(),SearchActivity.class);
                startActivity(intent);
                break;
            case R.id.tvEnterBankInfo:
                break;
            case R.id.main:
                break;
            case R.id.imgShare:
                Intent intent1 = new Intent(getContext(), SearchActivity.class);
                startActivity(intent1);
                break;
            case R.id.rlSendMoney:
                getFragmentManager().beginTransaction().replace(R.id.root, new FinishFragment()).addToBackStack("finish").commit();
                break;
        }
    }
}
