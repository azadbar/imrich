package im.rich.imrich.fragments;


import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.rich.imrich.R;
import im.rich.imrich.activities.SearchActivity;
import im.rich.imrich.helper.MacAddress;
import im.rich.imrich.interfaces.EnterNameInterface;
import im.rich.imrich.presenter.EnterNamePresenter;
import im.rich.imrich.retrofit.resObject.SearchResponse;
import im.rich.imrich.retrofit.resObject.StartPageResponse;

public class EnterNameFragment extends Fragment implements EnterNameInterface {


    @BindView(R.id.tvEnterName)
    AppCompatEditText tvEnterName;
    @BindView(R.id.tvNext)
    AppCompatTextView tvNext;
    @BindView(R.id.main)
    RelativeLayout main;
    @BindView(R.id.imgShare)
    AppCompatImageView imgShare;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.imgSearch)
    AppCompatImageView imgSearch;

    String name;
    String mac;

    private EnterNamePresenter mEnterNamePresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_secend_page, container, false);
        mEnterNamePresenter = new EnterNamePresenter(this, this);
        ButterKnife.bind(this, view);
        return view;
    }


    @OnClick({R.id.tvEnterName, R.id.tvNext, R.id.imgShare, R.id.imgSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.tvEnterName:
                break;
            case R.id.tvNext:

                getData();
                mEnterNamePresenter.callWepApi(mac, name);
//                getFragmentManager().beginTransaction().replace(R.id.root, new EnterBankAcountFragment()).addToBackStack("banckAccount").commit();
                break;
            case R.id.imgShare:
                shareApplication();
                break;
            case R.id.imgSearch:
//                mEnterNamePresenter.callSearchApi();
                Intent searchIntent = new Intent(getContext(), SearchActivity.class);
                getContext().startActivity(searchIntent);
                break;
        }
    }


    public void getData() {
        name = tvEnterName.getText().toString();
        mac = MacAddress.getMacAddress(getContext());
    }

    @Override
    public void onFailur() {

    }

    @Override
    public void StartPageSuccess(StartPageResponse obj) {
        openBrowser(obj);
    }

//    @Override
//    public void successResponse(SearchResponse obj) {
//        Intent intent = new Intent(getContext(), SearchActivity.class);
//        intent.putExtra("EnterSercch", obj);
//        getContext().startActivity(intent);
//    }

    private void openBrowser(StartPageResponse obj) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://sep.shaparak.ir/Payment.aspx?Amount=" + obj.getAmount() + "&MID=10968988&ResNum=" + obj.getId() + "&RedirectURL=https%3A%2F%2Fimrich.cf%2Fpayresult.php"));
        startActivity(browserIntent);
    }


    public void shareApplication() {
        ApplicationInfo app = getActivity().getApplicationContext().getApplicationInfo();
        String filePath = app.sourceDir;

        Intent intent = new Intent(Intent.ACTION_SEND);

        // MIME of .apk is "application/vnd.android.package-archive".
        // but Bluetooth does not accept this. Let's use "*/*" instead.
        intent.setType("*/*");

        // Append file and send Intent
        File originalApk = new File(filePath);

        try {
            //Make new directory in new location
            File tempFile = new File(getActivity().getExternalCacheDir() + "/ExtractedApk");
            //If directory doesn't exists create new
            if (!tempFile.isDirectory())
                if (!tempFile.mkdirs())
                    return;
            //Get application's name and convert to lowercase
            tempFile = new File(tempFile.getPath() + "/" + getActivity().getString(app.labelRes).replace(" ","").toLowerCase() + ".apk");
            //If file doesn't exists create new
            if (!tempFile.exists()) {
                if (!tempFile.createNewFile()) {
                    return;
                }
            }
            //Copy file to new location
            InputStream in = new FileInputStream(originalApk);
            OutputStream out = new FileOutputStream(tempFile);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            System.out.println("File copied.");
            //Open share dialog
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(tempFile));
            getActivity().startActivity(Intent.createChooser(intent, "Share app via"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
