package im.rich.imrich.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.rich.imrich.R;
import im.rich.imrich.activities.SearchActivity;

public class FinishFragment extends Fragment {


    @BindView(R.id.imgSearch)
    AppCompatImageView imgSearch;
    @BindView(R.id.tvCon)
    AppCompatTextView tvCon;
    @BindView(R.id.btnRich)
    AppCompatImageView btnRich;
    @BindView(R.id.imgDiamond)
    AppCompatImageView imgDiamond;
    @BindView(R.id.tvName)
    AppCompatTextView tvName;
    @BindView(R.id.imgShare)
    AppCompatImageView imgShare;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_finish, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.imgSearch, R.id.imgShare})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSearch:
                Intent intent = new Intent(getContext(), SearchActivity.class);
                startActivity(intent);
                break;
            case R.id.imgShare:
                break;
        }
    }
}
