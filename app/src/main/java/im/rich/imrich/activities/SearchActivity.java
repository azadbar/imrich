package im.rich.imrich.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.rich.imrich.R;
import im.rich.imrich.adapter.SearchAdapter;
import im.rich.imrich.interfaces.SearchInterface;
import im.rich.imrich.presenter.SearchPresenter;

public class SearchActivity extends AppCompatActivity implements SearchInterface, TextWatcher {


    private Context context;
    @BindView(R.id.edtSearch)
    EditText edtSearch;
    @BindView(R.id.rlSearch)
    RelativeLayout rlSearch;
    @BindView(R.id.rvList)
    RecyclerView rvList;

    private List<String> list;
    private SearchAdapter mAdapter;
    RecyclerView.LayoutManager layoutManager;

    private SearchPresenter mSearchPresenter;
    SearchAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        mSearchPresenter = new SearchPresenter(this, this);
        edtSearch.addTextChangedListener(this);
        mSearchPresenter.callWebAPI();
    }

    @OnClick(R.id.rlSearch)
    public void onViewClicked() {
    }


    @Override
    public void successSearch(Context currentContext, ArrayList<String> list) {
//        if (list.size() > 0){
//            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
//            rvList.setHasFixedSize(true);
//            rvList.setLayoutManager(linearLayoutManager);
//        }
    }

    @Override
    public void success(List<String> list) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(linearLayoutManager);
        this.list = list;
        adapter = new SearchAdapter(list);
        rvList.setAdapter(adapter);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        ArrayList<String> temp = new ArrayList<String>();
        for (int j = 0; j < list.size(); j++) {
            if (list.get(j).contains(charSequence)) {
                temp.add(list.get(j));
            }
        }
        adapter = new SearchAdapter(temp);
        rvList.setAdapter(adapter);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
