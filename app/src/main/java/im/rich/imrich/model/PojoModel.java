package im.rich.imrich.model;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;

import java.util.ArrayList;

import im.rich.imrich.controller.AppController;
import im.rich.imrich.presenter.EnterNamePresenter;
import im.rich.imrich.presenter.SearchPresenter;
import im.rich.imrich.retrofit.CallerService;
import im.rich.imrich.retrofit.ServerListener;
import im.rich.imrich.retrofit.myMethods;
import im.rich.imrich.retrofit.reqobjects.SearchReq;
import im.rich.imrich.retrofit.reqobjects.StartPageReq;
import im.rich.imrich.retrofit.resObject.SearchResponse;
import im.rich.imrich.retrofit.resObject.StartPageResponse;

/**
 * Created by mohammad on 2/20/2018 AD.
 */

public class PojoModel implements ServerListener {


    Context context;
    private ServerListener sl;
    private EnterNamePresenter enterNamePresenter;
    private SearchPresenter searchPresenter;

    PojoModel() {

    }

    public PojoModel(EnterNamePresenter enterNamePresenter) {
        this.enterNamePresenter = enterNamePresenter;
        sl = this;
    }

    public PojoModel(SearchPresenter richPresenter) {
        this.searchPresenter = richPresenter;
        sl = this;
    }

    @Override
    public void onFailure(int methodCode, String str) {

    }

    @Override
    public void onSuccess(int methodCode, JsonObject jsonObject) throws JSONException {

        if (methodCode == myMethods.Login.getMethodValue()) {
            loginResponceProcess(jsonObject);
        } else if (methodCode == myMethods.Search.getMethodValue()) {
            SearchResponseProcess(jsonObject);
        }

    }

//    private void SearchEnterNameResponseProcess(JsonObject jsonObject) {
//        Gson gson = new Gson();
//        JsonParser parser = new JsonParser();
//        JsonElement eJson = parser.parse(jsonObject.toString());
//        SearchResponse obj = gson.fromJson(eJson, SearchResponse.class);
//        enterNamePresenter.successSearch(obj);
//    }

    private void SearchResponseProcess(JsonObject jsonObject) {
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonElement eJson = parser.parse(jsonObject.toString());
        SearchResponse obj = gson.fromJson(eJson, SearchResponse.class);
        searchPresenter.successSearch(obj.getList());

    }




    private void loginResponceProcess(JsonObject jsonObject) {
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonElement eJson = parser.parse(jsonObject.toString());
        StartPageResponse obj = gson.fromJson(eJson, StartPageResponse.class);
        enterNamePresenter.successLogin(obj);

    }

    public void callWebApi(String mac, String name) {
        StartPageReq req = new StartPageReq(mac, name);
        CallerService.Login(sl, req);
    }

    public void callSearchApi() {
        SearchReq req = new SearchReq();
        CallerService.Search(sl, req);
    }

    public void callEnterName() {
        SearchReq req = new SearchReq();
        CallerService.SearchEnterName(sl, req);
    }
}
