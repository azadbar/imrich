package im.rich.imrich.retrofit.resObject;

/**
 * Created by mohammad on 2/20/2018 AD.
 */

public class StartPageResponse {


    private int id;
    private String amount;

    public StartPageResponse(int id, String amount) {
        this.id = id;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public String getAmount() {
        return amount;
    }
}
