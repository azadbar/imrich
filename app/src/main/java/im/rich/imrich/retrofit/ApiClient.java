package im.rich.imrich.retrofit;

import android.util.Base64;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import im.rich.imrich.retrofit.Constants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    static Retrofit retrofit = null;


    public static Retrofit getClient() {





        OkHttpClient myClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            public Response intercept(Chain chain) throws IOException {
                Request.Builder ongoing;
                Request.Builder ongoing2;
                ongoing = chain.request().newBuilder().addHeader("Content-Type", "application/json; charset=utf-8");
                return chain.proceed(ongoing.build());
            }
        }).connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build();


        retrofit = new Builder().baseUrl(Constants.URL_API).addConverterFactory(GsonConverterFactory.create()).client(myClient).build();

        return retrofit;
    }

}
