package im.rich.imrich.retrofit;


import com.google.gson.JsonObject;

import im.rich.imrich.retrofit.reqobjects.SearchReq;
import im.rich.imrich.retrofit.reqobjects.StartPageReq;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface reqInterface {

    @POST("/start.php")
    Call<JsonObject> start(@Body StartPageReq req);

    @GET("/list.php")
    Call<JsonObject> serach();
}
