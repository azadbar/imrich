package im.rich.imrich.retrofit.reqobjects;



/**
 * Created by mohammad on 2/20/2018 AD.
 */

public class StartPageReq {

    private String mac;
    private String name;

    public StartPageReq(String mac, String name) {
        this.mac = mac;
        this.name = name;
    }

    public String getMac() {
        return mac;
    }

    public String getName() {
        return name;
    }
}
