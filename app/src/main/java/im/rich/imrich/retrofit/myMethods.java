package im.rich.imrich.retrofit;

public enum myMethods {
    Login("Login", 0),
    Search("Search",1 ),
    SearchEnterName("SearchEnterName",2);


    private String methodName;
    private int methodValue;

    public int getMethodValue() {

        return this.methodValue;
    }

    private myMethods(String toString, int value) {
        this.methodName = toString;
        this.methodValue = value;
    }

    public String toString() {

        return this.methodName;
    }
}
