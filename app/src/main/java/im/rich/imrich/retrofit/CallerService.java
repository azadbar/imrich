package im.rich.imrich.retrofit;


import im.rich.imrich.retrofit.reqobjects.SearchReq;
import im.rich.imrich.retrofit.reqobjects.StartPageReq;

public class CallerService {


    public static void Login(ServerListener sl, StartPageReq req) {
       new ServerResponse().setCall(myMethods.Login, ((reqInterface) ApiClient.getClient().create(reqInterface.class)).start(req),sl);
    }

    public static void Search(ServerListener sl, SearchReq req) {
        new ServerResponse().setCall(myMethods.Search, ((reqInterface) ApiClient.getClient().create(reqInterface.class)).serach(),sl);
    }

    public static void SearchEnterName(ServerListener sl, SearchReq req) {
        new ServerResponse().setCall(myMethods.SearchEnterName, ((reqInterface) ApiClient.getClient().create(reqInterface.class)).serach(),sl);
    }
}
