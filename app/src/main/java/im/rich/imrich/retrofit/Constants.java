package im.rich.imrich.retrofit;

import android.content.Context;
import android.os.Environment;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;

import im.rich.imrich.helper.PreferencesData;


public class Constants {
    public static String Key = "vrs@#R#$_01^!*F@#DS#";
    //public static String URL_API = "http://172.16.25.115:8080/"; //local amoo bohlool;
    public static String URL_API = "https://imrich.cf";
    //public static String URL_API =  "http://172.16.25.111:9090/"; -C> local bayat
    public static String URL_AGENCY_LOGO = "http://www.delta.ir/Images/Agencies/ThumbnailImages/";
    public static String URL_BASE_PHOTO_THUMBNAIl = "http://www.delta.ir/Images/Deposit/ThumbnailImages/";
    public static String URL_BASE_PHOTO = "http://www.delta.ir/Images/Deposit/Images/";
    //public static String URL_AGENCY_LOGO = "http://www.delta.ir/Images/Agencies/ThumbnailImages/";
    public static String URL_BASE_PHOTO_BEFORE_CONFIRM = "http://www.delta.ir/Images/Temp/Images/";
    public static File storage_Dir_Image = new File(
            Environment.getExternalStoragePublicDirectory(
                    Environment.getRootDirectory().getParent()
            ),
            "/Delta/Images"
    );

    public static File storage_Dir_File = new File(
            Environment.getExternalStoragePublicDirectory(
                    Environment.getRootDirectory().getParent()
            ),
            "/Delta/Files"
    );
    // online api address -> http://www.delta.ir
    // local api address - > http://172.16.25.115:8080
    public static String token = "";

    public static void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    public static String getUserID(Context context){
        return PreferencesData.getString( "userID");
    }

    public static boolean isResponseTrue(JsonObject obj){
        JsonElement je = obj.get("Successed");
        if (je.isJsonNull()) {
            return false;

        } else if (je.getAsBoolean()) {
            return true;
        } else {
            return false;
        }
    }
    public static boolean isUserAuthorized(JsonObject obj){
        JsonElement je = obj.get("NotAuthorizedUser");
        if (je.isJsonNull()) {
            return false;

        } else if (je.getAsBoolean()) {
            return true;
        } else {
            return false;
        }
    }

}
