package im.rich.imrich.retrofit.resObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class SearchResponse implements Serializable {

    @SerializedName("success")
    private boolean success;

    @SerializedName("list")
    private ArrayList<String> list;

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public List<String> getList() {
        return list;
    }
}